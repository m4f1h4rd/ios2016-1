
#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellID = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%lu, %lu", indexPath.section, indexPath.row];
    
    return cell;
}

// метод определяет, какой хидер будет у секции (не путать с хидером таблицы!!).
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [NSString stringWithFormat:@"section header %lu", section];
}

// метод определяет, какой футер будет у секции (не путать с футером таблицы!!)
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return [NSString stringWithFormat:@"section footer %lu", section];
}

// отображение кастомных UIView в хидерах и футерах секции
// для этого нужно переоределить метод по созданию view и метод который вернет высоту для кастомной view (по дефолту высота хидера и фурет view равна 20 пунктов)
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = nil;
    
    if (section == 2)
    {
        view = [[UIView alloc] init];
        view.backgroundColor = [UIColor redColor];
    }
    
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = nil;
    
    if (section == 2)
    {
        view = [[UIView alloc] init];
        view.backgroundColor = [UIColor greenColor];
    }
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return section == 2 ? 100 : 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return section == 2 ? 120 : 20;
}


#pragma mark - UITableViewDelegate

@end
