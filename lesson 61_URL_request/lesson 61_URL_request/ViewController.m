//
//  ViewController.m
//  lesson 61_URL_request
//
//  Created by Yuriy Bosov on 10/3/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"
#import "Film+CoreDataClass.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // start request
    NSURL *url = [NSURL URLWithString:@"http://api.kinopoisk.cf/getTodayFilms"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
    {
        if (!error && data) {
            NSDictionary *obj = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
            if ([obj isKindOfClass:[NSDictionary class]]) {
                NSArray *filmsData = [obj objectForKey:@"filmsData"];
                
                // удалить все текущие записи!!!
                
                
                if ([filmsData isKindOfClass:[NSArray class]]) {
                    
                    for (NSDictionary *filmData in filmsData) {
                        
                        NSEntityDescription *ed = [NSEntityDescription entityForName:@"Film" inManagedObjectContext:context()];
                        
                        Film *film = [[Film alloc] initWithEntity:ed insertIntoManagedObjectContext:context()];
                        
                        film.identifier = [filmData objectForKey:@"id"];
                        film.nameRU = [filmData objectForKey:@"nameRU"];
                        film.nameEN = [filmData objectForKey:@"nameEN"];
                        film.isIMAX = [filmData objectForKey:@"isIMAX"];
                        // распарсили сырые данные, теперь можно создавать модели
                    }
                    saveContext();
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self showAllFilms];
                    });
                    
                }
                
            }
            
        } else{
            NSLog(@"error %@", error);
        }
    }];
    
    [task resume];
}

- (void)showAllFilms {
    NSFetchRequest *request = [Film fetchRequest];
    NSArray *result = [context() executeFetchRequest:request error:nil];
    NSLog(@"result %lu, \n%@", result.count, result);
    
    Film *f = result.firstObject;
    NSLog(@"%@ %@",f.identifier, f.nameRU);
}

@end
