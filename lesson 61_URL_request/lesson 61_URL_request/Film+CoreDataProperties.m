//
//  Film+CoreDataProperties.m
//  lesson 61_URL_request
//
//  Created by Yuriy Bosov on 10/3/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Film+CoreDataProperties.h"

@implementation Film (CoreDataProperties)

+ (NSFetchRequest<Film *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Film"];
}

@dynamic type;
@dynamic identifier;
@dynamic nameRU;
@dynamic nameEN;
@dynamic year;
@dynamic isIMAX;
@dynamic isNew;
@dynamic posterURL;
@dynamic premiereRU;
@dynamic videoURL_HD;
@dynamic videoURL_SD;
@dynamic videoURL_LOW;

@end
