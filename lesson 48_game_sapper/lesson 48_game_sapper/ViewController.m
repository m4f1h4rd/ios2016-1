//
//  ViewController.m
//  lesson 48_game_sapper
//
//  Created by Yuriy Bosov on 7/20/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"
#import "GameFiled.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [(GameFiled *)self.view setupGameItems];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gameWin) name:@"gameWin" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gameOwer) name:@"gameOwer" object:nil];
}

#pragma mark - Actions

- (IBAction)refrefButtonClicked:(id)sender {
    [(GameFiled *)self.view refreshGameItems];
}

- (IBAction)changeLevel:(UISegmentedControl *)sender {
    
    
}
#pragma mark - Alert Show

- (void)showAlectWithTitle:(NSString *)title message:(NSString *)message {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *actionNewGame = [UIAlertAction actionWithTitle:@"new game" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [(GameFiled *)self.view refreshGameItems];
        
    }];
    
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:actionNewGame];
    [alert addAction:actionOk];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark - Notifications

- (void)gameWin {
    [self showAlectWithTitle:@"You Win!!!" message:@":)"];
}

- (void)gameOwer {
    [self showAlectWithTitle:@"You Loose!!!" message:@":("];
}

@end
