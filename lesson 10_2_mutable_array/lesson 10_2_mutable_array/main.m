#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{
    @autoreleasepool
    {
        // динамический массив
        // 1. создаем пустой динамический массив
        NSMutableArray *array = [[NSMutableArray alloc] init];
        // 2. добавляем в него элементы
        [array addObject:@(100)];
        [array addObject:@(-100)];
        
        NSString *str = @"qwerqwer";
        [array addObject:str];
        NSLog(@"array %@", array);
        
        // 3. удаление элемента по индексу (важно не выйти за перделы массива)
        NSUInteger index = 2;
        if (index < array.count)
        {
            [array removeObjectAtIndex:index];
            NSLog(@"array %@", array);
        }
        else
        {
            NSLog(@"выход за переделы массива");
        }
        
        // 4. удаление элемента по объекту
        [array removeObject:str];
        NSLog(@"array %@", array);
        
        /////// хитрое практическое задание
        // создать большой массив, а потом оставить в нем всего лишь 5 элементов
        // удаление всех элементов массива
        [array removeAllObjects];
        
        // наполнение массива 10ми элеметов в цикле
        for (NSUInteger i = 0; i < 100; i++)
        {
            [array addObject:@(i)];
        }
        NSLog(@"кол-во элементов в массиве = %lu", array.count);
        // удаляем 95 элементов с конца (что бы осталось 5ть первых элементов)
        NSRange range = NSMakeRange(5, array.count - 5);
        [array removeObjectsInRange:range];
        
        NSLog(@"кол-во элементов в массиве = %lu", array.count);
        NSLog(@"массив = %@", array);
    }
    return 0;
}
