//
//  ATMModel.m
//  lesson_75_BEST_BestPractic_GoogleMAP_PrivateBank
//
//  Created by Yurii Bosov on 11/21/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ATMModel.h"

@implementation ATMModel

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc]
            initWithModelToJSONDictionary:@{@"placeName":@"placeRu",
                                            @"fullAddress":@"fullAddressRu",
                                            @"latitude":@"latitude",
                                            @"longitude":@"longitude"
                                            }];
}

@end
