//
//  ATMTableController.m
//  lesson_75_BEST_BestPractic_GoogleMAP_PrivateBank
//
//  Created by Yurii Bosov on 11/21/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ATMTableController.h"
#import "ServerManager.h"
#import "ATMCell.h"
#import "ATMModel.h"

@interface ATMTableController () {
    NSArray < ATMModel *> *dataSources;
}

@end

@implementation ATMTableController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[ServerManager sharedManager] ATMFromCity:@"Днепр" withComplitionBlock:^(NSArray *data, NSError *error) {
        
        dataSources = data;
        [self.tableView reloadData];
        
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ATMCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ATMCell" forIndexPath:indexPath];
    
    cell.lbPlaceName.text = [dataSources objectAtIndex:indexPath.row].placeName;
    cell.lbAddress.text = [dataSources objectAtIndex:indexPath.row].fullAddress;
    cell.lbDistance.text = nil;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

@end
