

#import <Foundation/Foundation.h>
#import "CustomClass1.h"
#import "CustomClass2.h"

int main(int argc, const char * argv[])
{
    @autoreleasepool
    {
        CustomClass1 *obj1 = [[CustomClass1 alloc] init];
        CustomClass2 *obj2 = [[CustomClass2 alloc] init];
        
        obj1.customClass2 = obj2;
        obj2.customClass1 = obj1;
        
        NSLog(@"%@, %@", obj1, obj2);
    }
    return 0;
}
