//
//  ViewController.m
//  lesson 56_1 Compare_NSTread_GCD_NSOperationQueue
//
//  Created by Yuriy Bosov on 9/19/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    NSOperationQueue *operationQueue;
}

@end

@implementation ViewController

#pragma mark - Difficul
- (void)difficulMethodsWithParam:(id)object {
    
    // start
    CGFloat startTime = CACurrentMediaTime();
    
    for (NSInteger i = 0; i < 1000000000; i++) {
        ;
    }
    
    // end
    CGFloat endTime = CACurrentMediaTime();
    NSLog(@"%@ finished %f", object, endTime - startTime);
}

#pragma mark - Button Actions
- (IBAction)btn1:(id)sender{

}

- (IBAction)btn2:(id)sender{
    
}

- (IBAction)btn3:(id)sender{
    
}

@end
