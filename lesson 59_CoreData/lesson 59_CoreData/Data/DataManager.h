//
//  DataManager.h
//  lesson 59_CoreData
//
//  Created by Yuriy Bosov on 9/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@import CoreData;

@interface DataManager : NSObject {
    NSPersistentContainer *persistentContainer;
}

+ (DataManager *)sharedManager;

- (NSManagedObjectContext *)context;
- (void)saveContext;

@end
