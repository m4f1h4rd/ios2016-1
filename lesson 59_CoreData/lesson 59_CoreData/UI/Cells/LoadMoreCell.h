//
//  LoadMoreCell.h
//  lesson 59_CoreData
//
//  Created by Yuriy Bosov on 9/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadMoreCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activity;

@end
