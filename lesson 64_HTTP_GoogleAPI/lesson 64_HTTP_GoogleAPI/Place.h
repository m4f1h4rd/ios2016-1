//
//  Place.h
//  lesson 64_HTTP_GoogleAPI
//
//  Created by Yuriy Bosov on 10/12/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;

@interface Place : NSObject

@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *placeID;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

+ (Place *)placeWithData:(NSDictionary *)data;

@end
