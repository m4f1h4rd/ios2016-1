//
//  Human.m
//  lesson 17_1_NSNotifications_example
//
//  Created by Yuriy Bosov on 3/21/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Human.h"

@implementation Human

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        
        [nc addObserver:self
               selector:@selector(printStart:)
                   name:kNotificationPrintStart
                 object:nil];
        
        [nc addObserver:self
               selector:@selector(printEnd:)
                   name:kNotificationPrintEnd
                 object:nil];
        
        [nc addObserver:self
               selector:@selector(printFailed:)
                   name:kNotificationPrintFailed
                 object:nil];
    }
    return self;
}

 - (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Notifications

- (void)printStart:(NSNotification*)aNotification
{
    NSLog(@"notification = %@", aNotification);
}

- (void)printEnd:(NSNotification*)aNotification
{
    NSLog(@"notification = %@", aNotification);
}

- (void)printFailed:(NSNotification*)aNotification
{
    NSLog(@"notification = %@", aNotification);
}

@end
