//
//  Printer.h
//  lesson 17_1_NSNotifications_example
//
//  Created by Yuriy Bosov on 3/21/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

@interface Printer : NSObject

@property (nonatomic, assign) NSUInteger currentCountPages;

- (void)startPrint:(NSUInteger)countPages;
- (void)endPrint;

@end
