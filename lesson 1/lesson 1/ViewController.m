//
//  ViewController.m
//  lesson 1
//
//  Created by Yuriy Bosov on 1/25/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.label.text = @"title";
    
}

- (void)buttonDidPressed
{
    count++; // count = count + 1;
    
    NSString *string = [NSString stringWithFormat:@"%i", count];
    self.label.text = string;
    
    NSLog(@"!!!!");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
