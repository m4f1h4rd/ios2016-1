//
//  MyClass.h
//  lesson16_protocol
//
//  Created by Yuriy Bosov on 3/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyProtocol.h"

// кастомный класс
@interface MyClass : NSObject <MyProtocol>

- (void)methodByMyClass;

@end
