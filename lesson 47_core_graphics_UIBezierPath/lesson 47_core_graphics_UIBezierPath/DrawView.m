//
//  DrawView.m
//  lesson 47_core_graphics_UIBezierPath
//
//  Created by Yuriy Bosov on 7/18/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "DrawView.h"

//void fillArc(UIBezierPath *path, CGPoint center, UIColor *fillColor);

void fillArc(UIBezierPath *path, CGPoint center, UIColor *fillColor)
{
    [path addLineToPoint:center];
    [path closePath];
    [fillColor setFill];
    [path fill];
}

@implementation DrawView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)rect {
    // цвет заливки
    [[UIColor blueColor] setFill];
    // цвет контура
    [[UIColor redColor] setStroke];
    
    ////////// прямоугольник
    UIBezierPath *pathRect = [UIBezierPath bezierPathWithRect:CGRectMake(20, 20, 100, 100)];
    pathRect.lineWidth = 2;
    
    // рисуем заливку
    [pathRect fill];
    // рисуем контур
    [pathRect stroke];
    
    //////////  круг
    UIBezierPath *pathCircle = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(140, 20, 100, 100)];
    [pathCircle fill];
    [pathCircle stroke];
    
    //////////  закругленный прямоугольник
    UIBezierPath *pathRoundedRect = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(20, 140, 100, 100) cornerRadius:10];
    [pathRoundedRect fill];
    [pathRoundedRect stroke];
    
    //////////  закругленный в некоторых углах прямоугольник
    pathRoundedRect = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(140, 140, 100, 100)
                                            byRoundingCorners:UIRectCornerTopLeft | UIRectCornerBottomRight  cornerRadii:CGSizeMake(50, 10)];
    [pathRoundedRect fill];
    [pathRoundedRect stroke];
    
    //////////  круговая диаграма из 5 секций
    UIBezierPath *pathArc1 = [UIBezierPath bezierPathWithArcCenter:CGPointMake(70, 310) radius:50 startAngle:0 endAngle:1 clockwise:YES];
    fillArc(pathArc1, CGPointMake(70, 310), [UIColor blueColor]);
    
    UIBezierPath *pathArc2 = [UIBezierPath bezierPathWithArcCenter:CGPointMake(70, 310) radius:50 startAngle:1 endAngle:2.2 clockwise:YES];
    fillArc(pathArc2, CGPointMake(70, 310), [UIColor cyanColor]);
    
    UIBezierPath *pathArc3 = [UIBezierPath bezierPathWithArcCenter:CGPointMake(70, 310) radius:50 startAngle:2.2 endAngle:4.1 clockwise:YES];
    fillArc(pathArc3, CGPointMake(70, 310), [UIColor orangeColor]);
    
    UIBezierPath *pathArc4 = [UIBezierPath bezierPathWithArcCenter:CGPointMake(70, 310) radius:50 startAngle:4.1 endAngle:4.9 clockwise:YES];
    fillArc(pathArc4, CGPointMake(70, 310), [UIColor magentaColor]);
    
    UIBezierPath *pathArc5 = [UIBezierPath bezierPathWithArcCenter:CGPointMake(70, 310) radius:50 startAngle:4.9 endAngle:0 clockwise:YES];
    fillArc(pathArc5, CGPointMake(70, 310), [UIColor greenColor]);
}

@end
