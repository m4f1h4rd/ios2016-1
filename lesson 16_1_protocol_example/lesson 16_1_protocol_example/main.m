//
//  main.m
//  lesson 16_1_protocol_example
//
//  Created by Yuriy Bosov on 3/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Truck.h"
#import "Dispatcher.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool
    {
        Dispatcher *dispatcher = [[Dispatcher alloc] init];
        
        Truck *truck1 = [[Truck alloc] init];
        truck1.truckName = @"VOLVO";
        truck1.dataSourceDelegate = dispatcher;
        
        [truck1 prepareToMove];
        [truck1 startMove];
        [truck1 endMove];
        
        Truck *truck2 = [[Truck alloc] init];
        truck2.truckName = @"MAN";
        truck2.dataSourceDelegate = dispatcher;
        
        [truck2 prepareToMove];
        [truck2 startMove];
        [truck2 endMove];
    }
    return 0;
}
