//
//  Truck.h
//  lesson 16_1_protocol_example
//
//  Created by Yuriy Bosov on 3/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TruckDataSourceProtocol.h"

@interface Truck : NSObject

@property (nonatomic, strong) NSString *truckName;  // название грузовика
@property (nonatomic, strong) NSString *destination;// пункт назначение
@property (nonatomic, assign) Cargo typeCargo;      // тип перевозимого груза

// объявление делегата, который отвечает за наполнение грузовика
@property (nonatomic, weak) id<TruckDataSourceProtocol> dataSourceDelegate;

- (void)prepareToMove;  // подготовка к движению
- (void)startMove;      // начать движение
- (void)endMove;        // закончить движение

- (NSString *)typeCargoToString:(Cargo)cargo;

@end
