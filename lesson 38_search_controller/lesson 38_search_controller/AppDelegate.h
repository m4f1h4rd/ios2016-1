//
//  AppDelegate.h
//  lesson 38_search_controller
//
//  Created by Yuriy Bosov on 6/15/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

