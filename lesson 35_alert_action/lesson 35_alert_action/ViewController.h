//
//  ViewController.h
//  lesson 35_alert_action
//
//  Created by Yuriy Bosov on 6/1/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    IBOutlet UITextField *textField;
    IBOutlet UIButton *clearButton;
}

@end

