//
//  ViewController.swift
//  lesson_82_swift_tableview
//
//  Created by Yurii Bosov on 12/19/16.
//  Copyright © 2016 ios. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var dataSource = ["1","2","3","4"]
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "TableView"
        self.tableView?.dataSource = self
        self.tableView?.delegate = self

    }
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = "cellid"
        let cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: cellId)
        
        cell?.textLabel?.text = dataSource[indexPath.row]
        
        var view: UIView! = cell.accessoryView
        if view == nil {
            view = UIView.init(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            
            view.backgroundColor = UIColor.red
            view.layer.cornerRadius = view.frame.size.width/2
            
            cell.accessoryView = view
        }

        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "openDetailedController", sender: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete {
            
            dataSource.remove(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic);
        }
    }
    
    
}

