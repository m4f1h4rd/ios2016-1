//
//  Note+CoreDataClass.m
//  lesson 58_CoreData2
//
//  Created by Yuriy Bosov on 9/23/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Note+CoreDataClass.h"
#import "Comment+CoreDataClass.h"
@implementation Note

- (NSArray *)commentsList {
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"createDate" ascending:YES];
    return [self.comments.allObjects sortedArrayUsingDescriptors:@[sd]];
}

@end
