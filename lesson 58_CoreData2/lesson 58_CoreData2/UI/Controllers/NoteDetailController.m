//
//  NoteDetailController.m
//  lesson 58_CoreData2
//
//  Created by Yuriy Bosov on 9/23/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "NoteDetailController.h"
#import "AppDelegate.h"
#import "CommentsController.h"
#import "CommetnDetailController.h"

@interface NoteDetailController () <UITextFieldDelegate, UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UIButton *commentsButton;

@end

@implementation NoteDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.note ? @"Note Detail" :  @"Create Note";
    
    // если у нас создание новой записи то note будет nil
    // note нужно создать
    if (!self.note) {
        NSEntityDescription *ed = [NSEntityDescription entityForName:NSStringFromClass([Note class]) inManagedObjectContext:managedObjectContext()];
        
        self.note = [[Note alloc] initWithEntity:ed insertIntoManagedObjectContext:nil];
    }
    
    self.textField.text = self.note.title;
    self.textView.text = self.note.body;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.commentsButton setTitle:[NSString stringWithFormat:@"Comments (%lu)", self.note.comments.count] forState:UIControlStateNormal];
}

#pragma mark - Actions

- (IBAction)textFieldDidChangeText:(UITextField *)sender{
    // change note title
}

- (IBAction)commentsButtonClicked:(UIButton *)sender {
    [self.view endEditing:YES];
    
    if (self.note.comments.count == 0){
        // открываем экран создание коммента
        CommetnDetailController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CommetnDetailController"];
        vc.note = self.note;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        // открываем экран co списком комментов
        CommentsController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentsController"];
        vc.note = self.note;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (IBAction)saveButtonClicked:(UIButton *)sender {
    [self.view endEditing:YES];
    
    if (self.textField.text.length == 0) {
#warning TODO add show alet
        NSLog(@"title не должен быть пустым");
        return;
    }
    
    //
    self.note.title = self.textField.text;
    self.note.body = self.textView.text;
    
    // если у нас новый note
    if (!self.note.managedObjectContext) {
        
        self.note.createDate = [NSDate date];
        self.note.modifyDate = self.note.createDate;
        
        [managedObjectContext() insertObject:self.note];
        if (self.note.comments.count) {
            for (Comment *commet in self.note.comments) {
                [managedObjectContext() insertObject:commet];
                commet.note = self.note;
            }
        }
    }else {
        self.note.modifyDate = [NSDate date];
    }
    saveContext();
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [_textView becomeFirstResponder];
    return YES;
}

#pragma mark - UITextViewDelegate 

- (void)textViewDidChange:(UITextView *)textView {
    // change note's body
}

@end
