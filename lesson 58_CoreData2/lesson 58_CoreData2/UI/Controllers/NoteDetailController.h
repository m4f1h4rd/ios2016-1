//
//  NoteDetailController.h
//  lesson 58_CoreData2
//
//  Created by Yuriy Bosov on 9/23/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Note+CoreDataClass.h"
#import "Comment+CoreDataClass.h"

@interface NoteDetailController : UIViewController

@property (nonatomic, strong) Note *note;

@end
