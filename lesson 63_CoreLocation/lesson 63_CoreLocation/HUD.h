//
//  HUD.h
//  lesson 63_CoreLocation
//
//  Created by Yuriy Bosov on 10/10/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HUD : UIView
{
    UIView *bgView;
    UIActivityIndicatorView *indicator;
}

- (void)showInView:(UIView *)view;
- (void)hide;

@end
