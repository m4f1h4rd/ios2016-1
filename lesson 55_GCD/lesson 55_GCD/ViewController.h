//
//  ViewController.h
//  lesson 55_GCD
//
//  Created by Yuriy Bosov on 9/16/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, copy) void(^testBlock)(void);
@end

