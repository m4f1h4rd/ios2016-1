//
//  ViewController.m
//  lesson 29_1_animations
//
//  Created by Yuriy Bosov on 5/11/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self animation];
}

- (void)animation
{
    self.circle.alpha = 1;
    self.circle.frame = CGRectZero;
    self.circle.center = CGPointMake(self.view.frame.size.width/2,
                                     self.view.frame.size.height/2);
    self.circle.layer.cornerRadius = 0;
    
    NSTimeInterval duration = 5;
    
    CGRect frame = self.circle.frame;
    frame.size.height = MAX(self.view.frame.size.width,
                            self.view.frame.size.height);
    frame.size.width = frame.size.height;
    
    __weak ViewController *weakSelf = self;
    
    [UIView animateWithDuration:duration animations:^{
        
        weakSelf.circle.alpha = 0.5;
        weakSelf.circle.frame = frame;
        weakSelf.circle.center = CGPointMake(weakSelf.view.frame.size.width/2,
                                         weakSelf.view.frame.size.height/2);
        
    } completion:^(BOOL finished) {
        [weakSelf animation];
    }];
    
    CGFloat estimateCorner = frame.size.width / 2;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"cornerRadius"];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    animation.fromValue = @(0);
    animation.toValue = @(estimateCorner);
    animation.duration = duration;
    [self.circle.layer setCornerRadius:estimateCorner];
    [self.circle.layer addAnimation:animation forKey:@"cornerRadius"];
}

@end
