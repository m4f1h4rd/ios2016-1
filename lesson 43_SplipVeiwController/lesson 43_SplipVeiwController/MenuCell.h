//
//  MenuCell.h
//  lesson 43_SplipVeiwController
//
//  Created by Yuriy Bosov on 7/4/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuModel.h"

@interface MenuCell : UITableViewCell

@property (nonatomic, strong) MenuModel *model;
@property (nonatomic, weak) IBOutlet UIView *separatorTop;
@property (nonatomic, weak) IBOutlet UIView *separatorBottom;

- (void)setupMenuModel:(MenuModel *)model;

@end
