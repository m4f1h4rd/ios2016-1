//
//  ViewController.swift
//  lesson_83_Pods
//
//  Created by Vlad on 21.12.16.
//  Copyright © 2016 STEP. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    
    var activity: HUD = HUD()
    var manager = AFHTTPSessionManager(baseURL: URL(string: "https://api.privatbank.ua/p24api/"))
    var dataSources = [Model]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        
        self.fetchData()
    }
    
    @IBAction func reloadButtonClicked() {
        self.fetchData()
    }
    
    func fetchData() {
        activity.show(in: self.view)
        
        manager.get("pubinfo?json&exchange&coursid=5",
                    parameters: nil,
                    progress: nil,
                    success: { (task: URLSessionDataTask, data: Any?) in
                        
                        self.activity.hide()
                        
                        if let modelsArray: [[String:Any]] = data as! [[String : Any]]? {
                            
                            self.dataSources = Model.modelsFrom(arrayDictionaries: modelsArray)
                            self.tableView.reloadData()
                        }
                        
        },
                    failure: { (task: URLSessionDataTask?, error: Error) in
                        self.activity.hide()
                        print("Error - \(error.localizedDescription)")
        })
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID")
        cell?.textLabel?.text = dataSources[indexPath.row].description()
        
        return cell!
    }
}

