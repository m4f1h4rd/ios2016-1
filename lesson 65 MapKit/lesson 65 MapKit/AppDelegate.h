//
//  AppDelegate.h
//  lesson 65 MapKit
//
//  Created by Vlad on 17.10.16.
//  Copyright © 2016 STEP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

