//
//  ViewController.m
//  lesson 56_NSOperations
//
//  Created by Yuriy Bosov on 9/19/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    NSOperationQueue *queue;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    queue = [[NSOperationQueue alloc] init];
    // задаем макс. кол-во паралельно выполнимых операций
    queue.maxConcurrentOperationCount = 3;
}

#pragma mark - Difficul
- (void)difficulMethodsWithParam:(id)object {
    
    // start
    CGFloat startTime = CACurrentMediaTime();
    
    for (NSInteger i = 0; i < 1000000000; i++) {
        ;
    }
    
    // end
    CGFloat endTime = CACurrentMediaTime();
    NSLog(@"%@ finished %f", object, endTime - startTime);
}

- (void)difficulMethodsEnded {
    __weak typeof(self) weakSelf = self;
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        // рандомно установим цвет для view
        weakSelf.view.backgroundColor = [UIColor colorWithHue:drand48() saturation:1.0 brightness:1.0 alpha:1.0];
    }];
}

#pragma mark - Button Actions

- (IBAction)startButtonClicked:(id)sender{
    
    __weak typeof(self) weakSelf = self;
    
    // создаем операции
    NSInvocationOperation *operation1 = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(difficulMethodsWithParam:) object:@"1"];
    // добавили блок, который вызывается по завершению операции
    [operation1 setCompletionBlock:^{
        [weakSelf difficulMethodsEnded];
    }];
    
    NSBlockOperation *operation2 = [[NSBlockOperation alloc] init];
    // добавили блок, в котором нужно выполнить "сложную" операцию
    [operation2 addExecutionBlock:^{
        [weakSelf difficulMethodsWithParam:@"2"];
    }];
    // добавили блок, который вызывается по завершению операции
    [operation2 setCompletionBlock:^{
        [weakSelf difficulMethodsEnded];
    }];
    
    NSBlockOperation *operation3 = [[NSBlockOperation alloc] init];
    // добавили блок, в котором нужно выполнить "сложную" операцию
    [operation3 addExecutionBlock:^{
        [weakSelf difficulMethodsWithParam:@"3"];
    }];
    // добавили блок, который вызывается по завершению операции
    [operation3 setCompletionBlock:^{
        [weakSelf difficulMethodsEnded];
    }];
    
    NSBlockOperation *operation4 = [[NSBlockOperation alloc] init];
    operation4.queuePriority = NSOperationQueuePriorityVeryHigh;
    // добавили блок, в котором нужно выполнить "сложную" операцию
    [operation4 addExecutionBlock:^{
        [weakSelf difficulMethodsWithParam:@"4"];
    }];
    // добавили блок, который вызывается по завершению операции
    [operation4 setCompletionBlock:^{
        [weakSelf difficulMethodsEnded];
    }];

    // ИЛИ добавление операций в очередь (по одной)
//    [queue addOperation:operation1];
//    [queue addOperation:operation2];
//    [queue addOperation:operation3];
//    [queue addOperation:operation4];
    
    // ИЛИ добавление массива операций в очередь
    [queue addOperations:@[operation1,operation2,operation3,operation4] waitUntilFinished:NO];
    
    // метод быстрого добавления NSBlockOperation
//    [queue addOperationWithBlock:^{
//        NSLog(@"addOperationWithBlock");
//        [weakSelf difficulMethodsEnded];
//    }];
    
    // nsoperationqueue completion all operations
    // создаем операцию, которая выполнется по завершению всех операций в очереди!!!
    NSBlockOperation *complishenOperation = [NSBlockOperation new];
    [complishenOperation setCompletionBlock:^{
        NSLog(@"queue finished all operations!!!");
    }];
    [complishenOperation addDependency:operation1];
    [complishenOperation addDependency:operation2];
    [complishenOperation addDependency:operation3];
    [complishenOperation addDependency:operation4];
    
    [queue addOperation:complishenOperation];
}

- (IBAction)cancelButtonClicked:(id)sender{
    if (queue.operationCount > 0) {
        NSLog(@"queue cancelAllOperations");
        [queue cancelAllOperations];
    }
}

@end
