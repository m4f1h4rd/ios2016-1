
#import "HardDrive.h"

@implementation HardDrive

- (NSString *)description
{
    return [NSString stringWithFormat:@"HardDrive: name = %@, speed = %lu, capacity = %lu", _name, _speed, _capacity];
}

@end
