//
//  Monitor.m
//  lesson 6_2
//
//  Created by Yuriy Bosov on 2/10/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Monitor.h"

@implementation Monitor

- (NSString *)description
{
    return [NSString stringWithFormat:@"Monitor: name = %@, diagonal = %0.1f", _name, _diagonal];
}

@end
