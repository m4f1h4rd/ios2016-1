//
//  CollectionViewCell.h
//  lesson 40_collectionView
//
//  Created by Yuriy Bosov on 6/22/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *label;

@end
