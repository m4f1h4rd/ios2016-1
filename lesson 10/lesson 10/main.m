
#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{
    @autoreleasepool
    {
        /////////////// сортировка строк
        // создаем массив из 4 строк
        NSArray *arrayOfStrings = @[@"qwer",@"asdfg",@"frea",@"zxcv"];
        
        // сортируем массив строк
        // с помощью метода -sortedArrayUsingSelector:
        // этот метод принимает в качестве аргумента селектор(метод),
        // по которому будет произведено сравнение и сортировка строк
        NSArray *sortedArrayOfStrings = [arrayOfStrings sortedArrayUsingSelector:@selector(compare:)];
        
        NSLog(@"arrayOfStrings          %@", arrayOfStrings);
        NSLog(@"sortedArrayOfStrings    %@", sortedArrayOfStrings);
        
        /////////////// сортировка чисел (NSNumber)
        // создаем массив чисел
        // сортируем аналогично как строки
        NSArray *arrayOfNumbers = @[@(200),@(-10),@(20),@(1000)];
        NSArray *sortedArrayOfNumbers = [arrayOfNumbers sortedArrayUsingSelector:@selector(compare:)];
        
        NSLog(@"arrayOfNumbers          %@", arrayOfNumbers);
        NSLog(@"sortedArrayOfNumbers    %@", sortedArrayOfNumbers);
    }
    return 0;
}
