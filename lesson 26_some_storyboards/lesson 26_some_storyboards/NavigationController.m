//
//  NavigationController.m
//  lesson 26_some_storyboards
//
//  Created by Yuriy Bosov on 4/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "NavigationController.h"

@interface NavigationController ()

@end

@implementation NavigationController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return [self.topViewController supportedInterfaceOrientations];
}

@end
