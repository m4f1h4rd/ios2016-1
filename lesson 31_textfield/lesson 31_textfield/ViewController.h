//
//  ViewController.h
//  lesson 31_textfield
//
//  Created by Yuriy Bosov on 5/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardToolBar.h"

@interface ViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
{
    NSArray *inputFields;
    NSArray *townsArray;
    
    KeyboardToolBar *toolBar;
    UIPickerView *pickerView;
}

@property (nonatomic, weak) IBOutlet UITextField *tf1;
@property (nonatomic, weak) IBOutlet UITextField *tf2;
@property (nonatomic, weak) IBOutlet UITextField *tf3;


@end

