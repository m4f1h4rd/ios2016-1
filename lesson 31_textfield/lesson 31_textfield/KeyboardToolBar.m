//
//  KeyboardToolBar.m
//  lesson 31_textfield
//
//  Created by Yuriy Bosov on 5/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "KeyboardToolBar.h"

@interface KeyboardToolBar ()

@property (nonatomic, weak) IBOutlet UIBarButtonItem *prevButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *nextButton;

@end

@implementation KeyboardToolBar


+ (KeyboardToolBar *)createToolBar
{
    return [[[NSBundle mainBundle] loadNibNamed:@"KeyboardToolBar"
                                         owner:nil
                                       options:nil] firstObject];
}

#pragma mark - Button Actions

- (IBAction)prevButtonPressed:(id)sender
{
    if ([_textFields containsObject:_currentTextField])
    {
        NSUInteger curentIndex = [_textFields indexOfObject:_currentTextField];
        if (curentIndex > 0)
        {
            UITextField *tf = [_textFields objectAtIndex:curentIndex - 1];
            [tf becomeFirstResponder];
        }
    }
}

- (IBAction)nextButtonPressed:(id)sender
{
    if ([_textFields containsObject:_currentTextField])
    {
        NSUInteger curentIndex = [_textFields indexOfObject:_currentTextField];
        if (curentIndex < _textFields.count - 1)
        {
            UITextField *tf = [_textFields objectAtIndex:curentIndex + 1];
            [tf becomeFirstResponder];
        }
    }
}

- (IBAction)doneButtonPressed:(id)sender
{
    [_currentTextField resignFirstResponder];
}

- (void)setCurrentTextField:(UITextField *)currentTextField
{
    _currentTextField = currentTextField;
    NSUInteger currentIndex = [_textFields indexOfObject:currentTextField];
    
    _prevButton.enabled = (currentIndex != 0);
    _nextButton.enabled = (currentIndex != _textFields.count - 1);
}

@end
