//
//  AppDelegate.h
//  lesson_78_AVCaptureSession
//
//  Created by Yurii Bosov on 11/30/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

