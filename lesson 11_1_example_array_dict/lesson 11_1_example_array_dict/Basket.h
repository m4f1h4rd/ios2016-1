//
//  Basket.h
//  lesson 11_1_example_array_dict
//
//  Created by Yuriy Bosov on 2/29/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Goods.h"

// класс продуктовая корзина

@interface Basket : NSObject
{
    // словарь, в котором мы будем хранить пары:
    // ключ - id товара
    // объект - массив товаров
    NSMutableDictionary *goodsIDs;
}

@property (nonatomic, assign) CGFloat money;

// добавление товара
- (void)addGoods:(Goods *)goods;

// удаление товара
- (void)removeGoods:(Goods *)goods;

// показать информацию по корзине (кол-во продуктов, цена)
- (void)showInfon;

// метод вернет сумму товаров на текущий момент
- (CGFloat)currentPrice;

@end
