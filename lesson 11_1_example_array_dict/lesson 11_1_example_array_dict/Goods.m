//
//  Goods.m
//  lesson 11_1_example_array_dict
//
//  Created by Yuriy Bosov on 2/29/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Goods.h"

@implementation Goods

+ (id)createGoodsWith:(NSString *)name
         categoryName:(NSString *)categoryName
                price:(NSNumber *)price
                   ID:(NSNumber *)ID
{
    Goods *goods = [[Goods alloc] init];
    
    goods.name = name;
    goods.categoryName = categoryName;
    goods.price = price;
    goods.ID = ID;
    
    return goods;
}

@end
