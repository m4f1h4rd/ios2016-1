//
//  InternetShop.m
//  lesson 14_repeat_oop
//
//  Created by Yuriy Bosov on 3/14/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "InternetShop.h"

@implementation InternetShop

- (id)initWithName:(NSString *)aName
{
    self = [super init];
    if (self)
    {
        name = aName;
        products = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)addProduct:(Product *)product
{
    NSMutableArray *listProducts = [products objectForKey:product.categoryName];
    
    if (listProducts == nil)
    {
        listProducts = [[NSMutableArray alloc] init];
        [products setObject:listProducts forKey:product.categoryName];
    }
    
    [listProducts addObject:product];
}

- (void)removeProduct:(Product *)product
{
    NSMutableArray *listProducts = [products objectForKey:product.categoryName];
    [listProducts removeObject:product];
    
    if (listProducts.count == 0)
    {
        [products removeObjectForKey:product.categoryName];
    }
}

- (void)showInfoShop
{
    NSArray *allProducts = [products allValues];
    
    NSUInteger productsCount = 0;
    CGFloat totalPrice = 0;
    
    for (NSArray *productsByCategory in allProducts)
    {
        productsCount += productsByCategory.count;
        
        for (Product *pr in productsByCategory)
        {
            totalPrice += [pr.price floatValue];
        }
    }
    NSLog(@"name - %@, общая сумма всех товаров - $%0.2f, общее кол-во товаров - %lu", name, totalPrice, productsCount);
}

- (void)showCategoryInfo
{
    NSArray *allCategoriesName = [products allKeys];
    allCategoriesName = [allCategoriesName sortedArrayUsingSelector:@selector(compare:)];
    
    for (NSString *categoryName in allCategoriesName)
    {
        NSArray *productsByCategory = [products objectForKey:categoryName];
        
        NSLog(@"%@, %lu %@",
              categoryName,
              productsByCategory.count,
              [self stringByCount:productsByCategory.count]);
    }
}

- (NSString *)stringByCount:(NSUInteger)count
{
    // 1 - товар
    // 2,3,4 - товара
    // ... - товаров
    NSString *str = @"товаров";
    
    if (count < 5 || count > 20)
    {
        if (count % 10 == 2 || count % 10 == 3 || count % 10 == 4)
        {
            str = @"товара";
        }
        else if (count % 10 == 1)
        {
            str = @"товар";
        }
    }
    
    return str;
}

- (NSArray *)findProductsByName:(NSString *)productName
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSArray *allProducts = [products allValues];
    for (NSArray *productsByCategory in allProducts)
    {
        for (Product *pr in productsByCategory)
        {
            NSRange range = [pr.name rangeOfString:productName];
            if (range.location != NSNotFound)
            {
                [result addObject:pr];
            }
        }
    }
    
    return result;
}

- (NSArray *)findProductsByCategoryName:(NSString *)categoryName
{
    return [products objectForKey:categoryName];
}

- (NSArray *)findProductsByTag:(NSString *)tag
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSArray *allProducts = [products allValues];
    
    // первый цикл - это перебор в массиве всех категорий товаро
    for (NSArray *productsByCategory in allProducts)
    {
        // второй цикл - это перебор товаров в массиве товаров конкретной категории
        for (Product *pr in productsByCategory)
        {
            // третий цикл - это перебор тегов в массиве тегов конкретного продукта
            for (NSString *productTag in pr.tags)
            {
                // 
                if ([productTag isEqualToString:tag])
                {
                    [result addObject:pr];
                    break;
                }
            }
        }
    }
    
    return result;
}

- (NSArray *)findProductsByCharacteristic:(NSString *)characteristic
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSArray *allProducts = [products allValues];
    
    // первый цикл - это перебор в массиве всех категорий товаро
    for (NSArray *productsByCategory in allProducts)
    {
        // второй цикл - это перебор товаров в массиве товаров конкретной категории
        for (Product *pr in productsByCategory)
        {
            // третий цикл - это перебор тегов в массиве тегов конкретного продукта
            for (NSString *productCharacteristic in pr.characteristics)
            {
                //
                if ([productCharacteristic isEqualToString:characteristic])
                {
                    [result addObject:pr];
                    break;
                }
            }
        }
    }
    
    return result;
}
@end
