//
//  MyTabBarController.m
//  lesson_72_ModalPresent
//
//  Created by Yurii Bosov on 11/7/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "MyTabBarController.h"

@interface MyTabBarController () <UITabBarControllerDelegate>

@end

@implementation MyTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
}

#pragma mark - UITabBarControllerDelegate

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    BOOL result = YES;
    if ([viewController.restorationIdentifier isEqualToString:@"modalVC"]) {
        result = NO;
        
        // Show modal view controller
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        UIViewController *vc = [storyboard instantiateInitialViewController];
        // для того чтобы показать модальное окно воспользуемся методом нмже:
        
        // настройка отображения модального окна и настройка перехода
        // насторйки для iPad
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
//             modalPresentationStyle - стиль отображение
//            vc.modalPresentationStyle = UIModalPresentationFormSheet;
            
            // - режим перехода
            vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        }
        
        [self presentViewController:vc animated:YES completion:nil];
    }
    return result;
}

@end
