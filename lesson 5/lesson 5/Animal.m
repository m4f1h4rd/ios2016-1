
#import "Animal.h"

@implementation Animal

+ (id)createObjectWithName:(NSString *)name
                   withAge:(NSUInteger)age
                withGender:(BOOL)gender
{
    // определение класса объекта
    Class objClass = [self class];
    
    // выделение паняти и инициализация под конкретный класс
    Animal *obj = [[objClass alloc] init];
    
    obj.name = name;
    obj.age = age;
    obj.gender = gender;
    
    return obj;
}

- (NSString *)description
{
    NSString *genderString = (self.gender == YES) ? @"male" : @"female";
    NSString *ageString = ([self isYoung]) ? @"young" : @"old";
    
    return [NSString stringWithFormat:@"name = %@, gender = %@, %@", self.name, genderString, ageString];
}

- (BOOL)isYoung
{
    BOOL value = (_age <= 5) ? YES : NO;
    
    return value;
}

@end
