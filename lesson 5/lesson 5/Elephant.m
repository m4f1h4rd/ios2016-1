
#import "Elephant.h"

@implementation Elephant

- (BOOL)isYoung
{
    BOOL value = (self.age <= 15) ? YES : NO;
    return value;
}

- (NSString *)description
{
    NSString *superDescription = [super description];
    return [NSString stringWithFormat:@"Elephant, %@", superDescription];
}

@end
