//
//  Object.m
//  lesson 41_UserDefault
//
//  Created by Yuriy Bosov on 6/29/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "MyObject.h"

@implementation MyObject

- (NSString *)description {
    return [NSString stringWithFormat:@"property1 = %@, property2 = %@", _property1, _property2];
}

#pragma mark - NSCoding

//encodeWithCoder: вызывается при сохрании объекта
- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:_property1 forKey:@"_property1"];
    [aCoder encodeObject:_property2 forKey:@"_property2"];
}

// initWithCoder: вызывается при load объекта
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super init];
    if (self) {
        _property1 = [aDecoder decodeObjectForKey:@"_property1"];
        _property2 = [aDecoder decodeObjectForKey:@"_property2"];
    }
    return self;
}

@end
