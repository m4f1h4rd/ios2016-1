//
//  AppDelegate.h
//  lesson 66_1_XML_TO_DICTIONARY
//
//  Created by Yurii Bosov on 10/19/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

