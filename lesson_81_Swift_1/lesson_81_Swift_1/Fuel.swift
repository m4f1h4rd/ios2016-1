//
//  Fuel.swift
//  lesson_81_Swift_1
//
//  Created by Yurii Bosov on 12/17/16.
//  Copyright © 2016 ios. All rights reserved.
//

import Foundation

class Fuel {
    var price = 0.0
    var name = ""
    
    func description() -> String {
        return String("name \(name), price $\(price)")
    }
}
