//
//  ViewController.swift
//  lesson_87_JokeApp
//
//  Created by Yurii Bosov on 1/21/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import JokeAPIKit
import PullToRefreshSwift

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, JokeCellProtocol {
    
    var currentSelectedModelView: JokeModelView?
    var currentSelectedIndexPath: IndexPath?
    var selectedCategoty: JokeCategoryModel?
    var needReloadDataAfterViewWillAppear = false
    
    var dataSource = [JokeModelView]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var headerLabel: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.changeCategoryNotification), name: NSNotification.Name(rawValue: "changeCategory"), object: nil)
        
        if let data = UserDefaults.standard.object(forKey: "category") as! Data! {
            self.selectedCategoty = NSKeyedUnarchiver.unarchiveObject(with: data) as! JokeCategoryModel?
        }
        
        self.tableView.addPullRefresh { [weak self] in
            self?.loadData()
        }
        
        self.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.needReloadDataAfterViewWillAppear {
            self.needReloadDataAfterViewWillAppear = false
            self.loadData()
        }
    }
    
    // MARK: - Notification
    
    func changeCategoryNotification(notification: Notification) {
        self.selectedCategoty = notification.object as! JokeCategoryModel?
        self.needReloadDataAfterViewWillAppear = true
    }
    
    // MARK: - Save Data
    
    func saveCategory() {
        if self.selectedCategoty != nil {
            
            let data = NSKeyedArchiver.archivedData(withRootObject: self.selectedCategoty!)
            UserDefaults.standard.set(data, forKey: "category")
        }
    }
    
    // MARK: - Load Data
    
    func loadData() {
        
        self.activity.startAnimating()
        
        if  self.selectedCategoty == nil {
            self.loadJokeCategories()
            self.headerLabel.text = "Загрузка данных..."
        } else {
            self.loadJokes(forCategories: self.selectedCategoty!)
            self.headerLabel.text = self.selectedCategoty?.desc
        }
    }
    
    func loadJokeCategories() {
        
        JokeCategoryModel.loadJokesCategories { (data:[JokeCategoryModel]?, error: String?) in
            
            if let categoriesData = data as [JokeCategoryModel]! {
                
                self.selectedCategoty = categoriesData.first
                self.saveCategory()
                
                self.loadJokes(forCategories: self.selectedCategoty!)
                self.headerLabel.text = self.selectedCategoty?.desc
                
            } else {
                
                self.activity.stopAnimating()
                self.tableView.stopPullRefreshEver()
                self.headerLabel.text = error
                
                self.dataSource.removeAll()
                self.tableView.reloadData()
            }
        }
    }
    
    func loadJokes(forCategories: JokeCategoryModel) {
        
        JokeModel.loadJokes(forCategoty: forCategories, count: 100) { (data: [JokeModel]?, error: String?) in
            
            self.activity.stopAnimating()
            self.tableView.stopPullRefreshEver()
            self.dataSource.removeAll()
            
            if let dataModels = data as [JokeModel]! {
                
                for model in dataModels {
                    let modelView = JokeModelView()
                    modelView.model = model
                    self.dataSource.append(modelView)
                }
                
            } else {
                self.headerLabel.text = error
            }
            
            self.tableView.reloadData()
        }
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! JokeCell
        
        cell.setup(modelView: dataSource[indexPath.row])
        cell.selectionStyle = .none
        cell.delegate = self
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if dataSource[indexPath.row].showAllContent {
            return UITableViewAutomaticDimension
        }
        else {
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if dataSource[indexPath.row].showAllContent {
            return UITableViewAutomaticDimension
        }
        else {
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let modelView = dataSource[indexPath.row]
        
        if currentSelectedIndexPath != nil &&
            currentSelectedModelView != nil{
            
            if currentSelectedIndexPath?.row == indexPath.row {
               currentSelectedModelView?.showAllContent = !(currentSelectedModelView?.showAllContent)!
            } else {
                currentSelectedModelView?.showAllContent = false
                currentSelectedModelView = modelView
                currentSelectedModelView?.showAllContent = true
            }
            
        } else {
            
            currentSelectedModelView = modelView
            currentSelectedModelView?.showAllContent = true
        }
        
        
        if currentSelectedIndexPath != nil {
        tableView.reloadRows(at: [indexPath, currentSelectedIndexPath!], with: UITableViewRowAnimation.automatic)
        } else {
            tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
        
        currentSelectedIndexPath = indexPath
    }
    
    //MARK: - JokeCellProtocol
    func jokeCellDidSharedButtonClicked(cell: JokeCell) {
        // open sher controller
//        print("\(cell.modelView?.model.shareURL())")
        
        let shareController = UIActivityViewController.init(activityItems: [(cell.modelView?.model.shareURL())!], applicationActivities: nil)
        
        self.present(shareController, animated: true, completion: nil)
    }
}

