//
//  JokeCell.swift
//  lesson_87_JokeApp
//
//  Created by Yurii Bosov on 1/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

protocol JokeCellProtocol : NSObjectProtocol {
    func jokeCellDidSharedButtonClicked(cell: JokeCell)
}


class JokeCell: UITableViewCell {

    var modelView: JokeModelView?
    weak var delegate: JokeCellProtocol?
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var buttonShare: UIButton!
    
    func setup(modelView: JokeModelView) {
        self.modelView = modelView
        self.labelText.attributedText = modelView.model.jokeAttributedText()
        self.buttonShare.isHidden = (self.modelView?.model.link == nil)
    }
    
    @IBAction func didSharedButtonClicked() {
        self.delegate?.jokeCellDidSharedButtonClicked(cell: self)
    }
}
