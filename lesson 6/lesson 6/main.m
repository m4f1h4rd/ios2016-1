
#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSString *str1 = @"qwer";
        NSString *str2 = @"asdf";
        NSString *str3 = @"qwerasdf";
        
        NSString *str = @"ooo";
        
        NSString *str4 = [NSString stringWithFormat:@"%@%@%@", str1,str2, str3];
        NSLog(@"%@", str4);
        
        // поиск подстроки в строке
        NSRange range = [str4 rangeOfString:str];
        
        // вывод NSRange в лог
        NSString *rangeStr = NSStringFromRange(range);
        
        if (range.location == NSNotFound)
        {
            NSLog(@"подстрока не найдета");
        }
        else
        {
            NSLog(@"range = %@", rangeStr);
        }
        
        // поиск строки по Ragne
        NSRange searchRage = NSMakeRange(10, 6);
        // проверка на выход за пределы строки
        if (str4.length >= searchRage.location + searchRage.length) // ???
        {
            NSString *searchString = [str4 substringWithRange:searchRage];
            NSLog(@"searchString = %@", searchString);
        }
        else
        {
            NSLog(@"Выход за пределы строки");
        }
        
        // сравнение строк с помощью метода isEqualToString
        NSString *str5 = @"qwerty";
        NSString *str6 = @"qwe";
        
        if ([str5 isEqualToString:str6])
        {
            NSLog(@"строка %@ равна строке %@", str5, str6);
        }
        else
        {
            NSLog(@"строка %@ НЕ равна строке %@", str5, str6);
        }
        
        // вывод строки в верхнем регистре
        NSLog(@"%@", [str4 uppercaseString]);
        
        // вывод строки с первым символом в верхнем регистре
        NSLog(@"%@", [str4 capitalizedString]);
    }
    return 0;
}
