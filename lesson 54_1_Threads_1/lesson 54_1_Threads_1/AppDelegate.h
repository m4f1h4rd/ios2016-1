//
//  AppDelegate.h
//  lesson 54_1_Threads_1
//
//  Created by Yuriy Bosov on 9/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

