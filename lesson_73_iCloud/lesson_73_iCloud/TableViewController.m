//
//  TableViewController.m
//  lesson_73_iCloud
//
//  Created by Yurii Bosov on 11/9/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "TableViewController.h"

@interface TableViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
    
    NSMutableArray *dataSources;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UITextField *textField;

@end

@implementation TableViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"iCloud";
    
    dataSources = [NSMutableArray new];
    
    NSArray *array = [[NSUbiquitousKeyValueStore defaultStore] arrayForKey:@"data"];
    if (array.count)
        [dataSources addObjectsFromArray:array];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(keyValueStoreDidChangeNotification)
                   name:NSUbiquitousKeyValueStoreDidChangeExternallyNotification
                 object:nil];
}

- (void)addNote:(NSString *)note {
    if (note.length) {
        self.textField.text = nil;
        [dataSources insertObject:note atIndex:0];
        [self.tableView reloadData];
        
        [[NSUbiquitousKeyValueStore defaultStore] setArray:dataSources
                                                    forKey:@"data"];
        BOOL b = [[NSUbiquitousKeyValueStore defaultStore] synchronize];
        
        if (b == NO) {
            NSLog(@"что то пошло не так с icloud :(");
        }
    }
}

#pragma mark - Button Actions

- (IBAction)addButtonClicked:(id)sender{
    [self.textField resignFirstResponder];
    [self addNote:self.textField.text];
}

#pragma mark - Notifications

- (void)keyValueStoreDidChangeNotification{
    
    NSArray *array = [[NSUbiquitousKeyValueStore defaultStore] arrayForKey:@"data"];
    
    [dataSources removeAllObjects];
    [dataSources addObjectsFromArray:array];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
    
    cell.textLabel.text = [dataSources objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self addNote:textField.text];
    return YES;
}

@end
