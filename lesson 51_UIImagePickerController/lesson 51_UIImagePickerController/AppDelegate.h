//
//  AppDelegate.h
//  lesson 51_UIImagePickerController
//
//  Created by Yuriy Bosov on 9/5/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

