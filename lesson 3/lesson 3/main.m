
#import <Foundation/Foundation.h>
#import "Animal.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool
    {
        // insert code here...
        
        // создаем переменную типа Animal
        // (создаем экземпляр класса Animal)
        // 1. указываем тип: 'Animal *'
        // 2. указываем название переменной: 'tigr'
        // 3. инициализация переменной: '='
        // 3.1 выделение памяти: '[Animal alloc]'
        // вызов статического метода alloc у класса
        // 3.2 инициализация объекта: '[ init]'
        Animal *tigr = [[Animal alloc] init];
        
        // зададим свойства переменной tigr
        // доступ к свойствам через '.' (точку)
        tigr.name = @"tigr №25";
        tigr.age = 3;
        tigr.gender = YES;
        
        [tigr showAnimalsInfo];
        [tigr addAnimalAge:2];
        [tigr showAnimalsInfo];
    }
    return 0;
}
