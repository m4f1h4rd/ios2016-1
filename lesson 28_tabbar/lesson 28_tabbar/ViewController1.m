//
//  ViewController.m
//  lesson 28_tabbar
//
//  Created by Yuriy Bosov on 4/27/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController1.h"

@implementation ViewController1

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.title = @"VC1";
    }
    return self;
}

@end
