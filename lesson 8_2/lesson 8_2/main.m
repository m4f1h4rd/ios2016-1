//
//  main.m
//  lesson 8_2
//
//  Created by Yuriy Bosov on 2/17/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool
    {
        NSArray *array = @[@(100),@(200),@(300)];
        
        // кол-во элементов в массиве
        NSUInteger count = array.count;
        NSLog(@"кол-во элементов в массиве = %lu", count);
        
        // доступ к элементу массива по индексу
        NSUInteger index = 2;
        if (index < count)
        {
            id tempObject = [array objectAtIndex:index];
            NSLog(@"%@", tempObject);
        }
        else
        {
            NSLog(@"Выход за пределы массива");
        }
        
        // доступ к элементу массива по индексу в сокращенном виде
        if (index < count)
        {
            id tempObject = array[index];
            NSLog(@"%@", tempObject);
        }
        else
        {
            NSLog(@"Выход за пределы массива");
        }
        
        // перебор массива с помощью цикла
        NSLog(@"перебор массива с помощью цикла");
        for (NSUInteger i = 0; i < count; i++)
        {
            id tempObject = array[i];
            NSLog(@"%@", tempObject);
        }
    }
    return 0;
}
