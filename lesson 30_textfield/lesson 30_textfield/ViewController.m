//
//  ViewController.m
//  lesson 30_textfield
//
//  Created by Yuriy Bosov on 5/16/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    inputFields = @[_tf1,_tf2,_tf3];
    
    _tf2.placeholder = @"please enter your email";
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tf3.frame.size.height, _tf3.frame.size.height)];
    leftView.backgroundColor = [UIColor redColor];
    leftView.userInteractionEnabled = NO;
    
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tf3.frame.size.height, _tf3.frame.size.height)];
    rightView.backgroundColor = [UIColor blueColor];
    rightView.userInteractionEnabled = NO;
    
    _tf3.leftView = leftView;
    _tf3.rightView = rightView;
    
    _tf3.leftViewMode = UITextFieldViewModeAlways;
    _tf3.rightViewMode = UITextFieldViewModeWhileEditing;
    
    UIView *keyboardView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    keyboardView.backgroundColor = [UIColor greenColor];
    _tf3.inputView = keyboardView;
    
    UIView *toolbarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolbarView.backgroundColor = [UIColor yellowColor];
    _tf3.inputAccessoryView = toolbarView;
    
    // Добавим обрадотчик нажатия на UIView
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidTaped)];
    [self.view addGestureRecognizer:recognizer];
}

- (void)viewDidTaped
{
    [self.view endEditing:YES];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"начали редактировать поле с тегом = %li", (long)textField.tag);
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"закончили редактировать поле с тегом = %li", (long)textField.tag);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
//    // по методике Павленко (актуален для джунов и при кол-ве текс-филдов < 5)
//    
//    if (textField == _tf1)
//    {
//        // becomeFirstResponder вызывается, если мы ходим начать редактировать это поле ввода (показать клаву)
//        [_tf2 becomeFirstResponder];
//    }
//    else if (textField == _tf2)
//    {
//        [_tf3 becomeFirstResponder];
//    }
//    else
//    {
//        // resignFirstResponder вызываем, если хотим закончить редактирование поля (спрятать клаву)
//        [textField resignFirstResponder];
//    }
    
    // второй метод
    
    if (inputFields.lastObject == textField)
    {
        [textField resignFirstResponder];
    }
    else if ([inputFields containsObject:textField])
    {
        NSUInteger index = [inputFields indexOfObject:textField];
        UITextField *nextTextField = [inputFields objectAtIndex:index + 1];
        [nextTextField becomeFirstResponder];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _tf3)
    {
        if (textField.text.length + string.length <= 10)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    return YES;
}

@end
