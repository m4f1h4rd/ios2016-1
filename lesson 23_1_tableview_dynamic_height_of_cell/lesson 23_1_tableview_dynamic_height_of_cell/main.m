//
//  main.m
//  lesson 23_1_tableview_dynamic_height_of_cell
//
//  Created by Yuriy Bosov on 4/11/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
