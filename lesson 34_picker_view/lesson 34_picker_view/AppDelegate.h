//
//  AppDelegate.h
//  lesson 34_picker_view
//
//  Created by Yuriy Bosov on 5/30/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

