//
//  MyProfileController.m
//  lesson67_FacebookSDK
//
//  Created by Yurii Bosov on 10/26/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "MyProfileController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface MyProfileController () {
    IBOutlet UILabel *lbFirstName;
    IBOutlet UILabel *lbLastName;
    IBOutlet UILabel *lbEmail;
    
    IBOutlet UIImageView *avatarView;
}

@end

@implementation MyProfileController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getUserProfile];
}

- (void)getUserProfile{
    NSDictionary *params = @{@"fields":@"email,gender,first_name,last_name,picture.type(large)"};

    // показали hud
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:params];

    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {

        // убрали hud
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if (error) {
            // show error alert
            NSLog(@"Error - %@", error.localizedDescription);
        } else {
            // show user info
            NSLog(@"Result - %@", result);
            lbFirstName.text = [result objectForKey:@"first_name"];
            lbLastName.text = [result objectForKey:@"last_name"];
            lbEmail.text = [result objectForKey:@"email"];
            
            NSString *pictureURLString = [result valueForKeyPath:@"picture.data.url"];
            if (pictureURLString) {
                [avatarView sd_setImageWithURL:[NSURL URLWithString:pictureURLString]
                              placeholderImage:nil
                                       options:SDWebImageRetryFailed];
            }
        }
    }];
}

@end
