//
//  FriendCell.m
//  lesson67_FacebookSDK
//
//  Created by Yurii Bosov on 10/26/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "FriendCell.h"

@implementation FriendCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
