//
//  Group.m
//  lesson_9
//
//  Created by Yuriy Bosov on 2/22/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Group.h"

@implementation Group

+ (Group *)createGroupWithName:(NSString *)name
                  withStudents:(NSArray *)students
{
    Group *obj = [[Group alloc] init];
    obj.name = name;
    obj.students = students;
    return obj;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"Group name: %@, students: %@", _name, _students];
}

- (NSUInteger)averageAge
{
    NSUInteger averageAge = 0;
    
    if (_students.count > 0)
    {
        NSUInteger summAge = 0;
        
        for (Student *st in _students)
        {
            summAge += st.age;
        }
        
        averageAge = summAge / _students.count;
    }
    
    return averageAge;
}

- (NSUInteger)countOfMan
{
    NSUInteger count = 0;
    
    for (Student *st in _students)
    {
        if (st.gender == YES)
        {
            count++;
        }
    }
    
    return count;
}

- (NSUInteger)countOfWoman
{
    NSUInteger count = 0;
    
    count = _students.count - [self countOfMan];
    
    return count;
}

- (NSString *)allNamesStudents
{
    NSString *allNames = nil;
    
    // получение из массива сложных объектов массива свойства с именем 'name'
    NSArray *arrayOfNames = [_students valueForKey:@"name"];
    
    // объединение элементов массива в одну строку с указанием разделителя
    allNames = [arrayOfNames componentsJoinedByString:@", "];
    
    return allNames;
}

- (Student *)searchStudentByName:(NSString *)name
{
    Student *student = nil;
    
    // TO DO
    for (Student *object in _students)
    {
        NSRange range = [object.name rangeOfString:name];
        if (range.location != NSNotFound)
        {
            student = object;
            break;
        }
    }
    
    return student;
}

// сортировка по имени
- (NSArray *)sortedStudentsByName
{
    // шаг 1 - создаем экземпляр класса NSSortDescriptor
    // NSSortDescriptor - класс которые задает условия сортировки (key - по какому свойству сортировать, ascending - по возрастанию\по убыванию)
    NSSortDescriptor *sortDescriptorByName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    
    // шаг 2 - сортируем наш массив студентов
    // с помощью метода -sortedArrayUsingDescriptors:
    // этот метод принимает массив условий сортировки
    // где,
    // arrayOfDescriptors - массив условий
    // sortedStudents - это уже отсортированный массив
    NSArray *arrayOfDescriptors = @[sortDescriptorByName];
    NSArray *sortedStudents = [_students sortedArrayUsingDescriptors:arrayOfDescriptors];
    
    return sortedStudents;
}

// сортировка по возрасту и имени
- (NSArray *)sortedStudentsByAgeAndName
{
    // условие сортировки по возрасту
    NSSortDescriptor *sortDescriptorByAge = [NSSortDescriptor sortDescriptorWithKey:@"age" ascending:YES];

    // условие сортировки по имени
    NSSortDescriptor *sortDescriptorByName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    
    // массив условий (очень важно соблюдать порядок добавления условий)
    NSArray *arrayOfDescriptors = @[sortDescriptorByAge, sortDescriptorByName];
    
    // получение отсортированого массива
    NSArray *sortedStudents = [_students sortedArrayUsingDescriptors:arrayOfDescriptors];
    
    return sortedStudents;
}

@end
