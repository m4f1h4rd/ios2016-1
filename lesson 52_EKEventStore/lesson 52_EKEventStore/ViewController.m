//
//  ViewController.m
//  lesson 52_EKEventStore
//
//  Created by Yuriy Bosov on 9/7/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"

@import EventKit;


@interface ViewController ()

@property (nonatomic, weak) IBOutlet UISwitch *localNotificationSwitch;

- (void)createEventInStore:(EKEventStore *)store;

@end


@implementation ViewController

- (IBAction)createEventButtonClicked:(id)sender {
    
    EKEventStore *store = [[EKEventStore alloc] init];
    
    // проверка авторизации на работу с календарем
    EKAuthorizationStatus status = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    switch (status) {
        case EKAuthorizationStatusNotDetermined:
        {
            __weak typeof(self) __weakSelf = self;
            // запрос на разрешение использовать календарь
            [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError * _Nullable error)
            {
                if (granted) {
                    // доступ дан, можно создавать событие
                    [__weakSelf createEventInStore:store];
                }
            }];
        }
            break;
            
        case EKAuthorizationStatusRestricted:
        case EKAuthorizationStatusDenied:
        {
            // пользователь запретил создавать события
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Предупреждение" message:@"Доступ на использование календаря запрещен. Для изменения достуна перейдите в настройки" preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                // открыть настройки телефона
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
            
        case EKAuthorizationStatusAuthorized:
        {
            // доступ дан, можно создавать событие
            [self createEventInStore:store];
        }
            break;
    }
}

- (void)createEventInStore:(EKEventStore *)store {
    
    // создание события
    EKEvent *event = [EKEvent eventWithEventStore:store];
    event.startDate = [NSDate dateWithTimeIntervalSince1970:1475226000];
    event.endDate = [NSDate dateWithTimeIntervalSince1970:1475269200];
    event.timeZone = [NSTimeZone systemTimeZone];
    
    NSLog(@"%@", event.eventIdentifier);
    
    event.title = @"Test Event";
    event.notes = @"Test notes";
    
    event.calendar = [store defaultCalendarForNewEvents];
    
    NSError *error = nil;
    [store saveEvent:event span:EKSpanThisEvent error:&error];

    if (error) {
        NSLog(@"error %@", error);
    } else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Событие успешно добавлено" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

@end
